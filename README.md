# Teste Frontend Leroy Merlin Brasil

## Escopo do Teste
O desenvolvimento do teste se baseia numa interface já existente, encontrada na pasta deste projeto. Uma dashboard simples para visualização/interação de links externos.

O objetivo é avaliar a capacidade de estimativa do(a) desenvolvedor(a), em conjunto com a capacidade de se trabalhar com uma interface inacabada, e bons padrões de código.

A interface deve ser construida se baseando na imagem disponibilizada (interface.png), e parte do teste representa o desenvolvedor correr atrás de alguns aspectos da interface:

- Família de ícones
- Tipografia
- Motion para interação com os elementos da interface
- Ou seja, só tem o png mesmo para se basear

### Responsividade da interface
A interface deve atender alguns critérios:

- Permitir filtrar as postagens pela barra de pesquisa (Se eu digitar algo, deve filtrar pelo o que foi digitado, caso não encontre nada, mostrar um feedback).
- Filtrar as postagens a partir de filtros pré determinados
  - Popularidade
  - Data
  - Comentários

### Construção da interface
Em critérios de tecnologia, é esperado:

- Consumir [os dados json](https://www.mocky.io/v2/5a6bc16631000078341b8b77) para popular os links na interface
- Bons padrões de código. (linters)
- Documentação de código.
- Testes automatizados (TDD/BDD)
- Automatizador de tarefas
- Crossbrowser (IEegde+, Chrome e FF)
- (OPCIONAL) Utilização de algum FW/Library para padronizar o fluxo de código

#### Observações
- O footer, links, loadmore, botão de menu, ícone de usuário não precisam ter interação
- Não tenha medo de fazer perguntas, de verdade.

## Solução

Segue instruções para baixar e rodar o projeto.

Faça o clone do projeto.
```
git clone https://rsoliveira87@bitbucket.org/rsoliveira87/reddit-like.git
```
Acesse a pasta do projeto.
```
cd reddit-like
```
A aplicação foi desenvolvida em NodeJS, para rodar, necessita ter o NodeJS instalado, se não tiver instalado, clique [aqui](https://nodejs.org/en/download/) para baixar.

Dentro da pasta projeto, execute o comando abaixo para instalar as dependências.
```
npm install
```
Depois execute o comando abaixo para iniciar o servidor.
```
nodemon app
```
Para visualizar a aplicação, acesse no navegador o endereço localhost:3000.

Utilizei gulp para automatizar as tarefas de compilar o SASS para CSS e minificar o CSS, para isso, execute no terminal o comando `gulp w`. Os arquivos compilados se encontram dentro da pasta app/assets/css.

#### Observação

- A busca procura pelo título, autor e URL.
- O arquivo app/services/mocky.js contém a classe que faz a requisição na API do Mocky.
- A aplicação possui apenas duas rotas, index e search e se encontram no diretório app/routes/.
- As controllers index e search se encontram no diretório app/controllers.
- As views index se encontram no diretório app/views.
